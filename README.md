Тестовое задание для Key Group  
==============================  
Необходимо подготовить простое приложение на Android, прислать скомпилированное приложение (apk) и ссылку на репозиторий.  
1. отображение карты google    
2. маркер текущего положения пользователя на карте поместить в центр экрана    
3. сделать поле, в котором отображается адрес под маркером  
4. прим скроле карты маркер остается на месте относительно экрана но сдвигается по карте, при этом занчение в поле с адресом под маркером обновляется  
5. кнопка возврата к текущему положению пользователя.  

## Сопроводительное пояснение  
Собранный apk лежит в `/app-debug.apk.qqq` (debug сборка). Нужно удалить рсширение .qqq  
Сборка и запуск:  
```sh
$./gradlew installDebug
```  
Приложение разрабатывалось по канонам [«чистой архитектуры»](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html) [Роберта Мартина](https://www.google.ru/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiskJuqnaPcAhXCCpoKHR9sCeYQFggoMAA&url=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%25D0%259C%25D0%25B0%25D1%2580%25D1%2582%25D0%25B8%25D0%25BD%2C_%25D0%25A0%25D0%25BE%25D0%25B1%25D0%25B5%25D1%2580%25D1%2582&usg=AOvVaw2vChYHc4z3P8q6FRPvXQ2Z) (Uncle Bob) с целью практики — «набить руку». В реальности для такой задачи это избыточно.   
Минимальная версия SDK 19 (KitKat).

### Используемые фреймворки
* [Google Maps](https://developers.google.com/android/guides/setup)
* [Butterknife](http://jakewharton.github.io/butterknife/)
* [Moxy](https://github.com/Arello-Mobile/Moxy)
* [Dagger2](https://google.github.io/dagger/)
* [RxJava2](http://reactivex.io/intro.html)
* [Timber](https://github.com/JakeWharton/timber)

### Что не сделано / недоделано
* Нет Unit-тестов
* Корявая обработка холодного старта
* Не проводилась оптимизация
* Не проводилось тестирования на memory leak (Leak Canary)
* Тестировалось только на одном устройстве
* Нет полноценной обработки ошибок
* Не занимался Pro Guard и отчетами lint
* Не оптимальное построение графа зависимостей Dagger
* Нужно переделать кастомную gradle-задачу incrementVersionCode — есть способ лучше.
