package com.keyg.test.app.data.repository;

public class UnknownLocationException extends RuntimeException 
{
	public UnknownLocationException(String message)
	{
		super(message);
	}
}
