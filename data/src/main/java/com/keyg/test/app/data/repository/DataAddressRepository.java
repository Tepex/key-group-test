package com.keyg.test.app.data.repository;

import android.location.Address;
import android.location.Geocoder;

import com.keyg.test.app.domain.Point;
import com.keyg.test.app.domain.repository.AddressRepository;

import io.reactivex.Single;

import java.util.List;

import timber.log.Timber;

public class DataAddressRepository implements AddressRepository
{
	public DataAddressRepository(Geocoder geocoder)
	{
		this.geocoder = geocoder;
	}
	
	@Override
	public Single<String> getAddressByLocation(Point point)
	{
		return Single.create(emitter -> 
		{
			List<Address> addresses = geocoder.getFromLocation(point.getLatitude(), point.getLongitude(), 1);
			if(emitter.isDisposed()) return;
			if(addresses == null || addresses.size() == 0)
				emitter.onError(new UnknownAddressException("Address is unknown"));
			else emitter.onSuccess(addresses.get(0).getAddressLine(0));
		});
	}
	
	private final Geocoder geocoder;
}
