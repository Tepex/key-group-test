package com.keyg.test.app.data.repository;

public class UnknownAddressException extends RuntimeException 
{
	public UnknownAddressException(String message)
	{
		super(message);
	}
}
