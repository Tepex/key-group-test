package com.keyg.test.app.data.repository;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;

import com.keyg.test.app.domain.Point;
import com.keyg.test.app.domain.repository.LocationRepository;

import io.reactivex.Single;

import timber.log.Timber;

public class DataLocationRepository implements LocationRepository
{
	public DataLocationRepository(FusedLocationProviderClient locationProviderClient,
                                  GoogleApiAvailability googleApiAvailability,
                                  Context context)
	{
		this.locationProviderClient = locationProviderClient;
		this.googleApiAvailability = googleApiAvailability;
		this.context = context;
	}
	
	@Override
	public Single<Point> getCurrentLocation()
	{
		//return Single.just(new Point(54.755980d, 37.628444d));
		return getLastLocation().map(location -> 
			new Point(location.getLatitude(), location.getLongitude()));
	}
	
	@Override
	public boolean checkPlayServicesAvailable()
	{
		final int status = googleApiAvailability.isGooglePlayServicesAvailable(context);
		if(status != ConnectionResult.SUCCESS) isGoogleApiAvailable = false;
		return isGoogleApiAvailable;
	}
	
	@Override
	public int getPlayServicesStatus()
	{
		return googleApiAvailability.isGooglePlayServicesAvailable(context);
	}
	
	private Single<Location> getLastLocation()
	{
		return Single.create(emitter -> 
		{
			try
			{
				locationProviderClient.getLastLocation()
					.addOnSuccessListener(location -> 
					{
						if(emitter.isDisposed()) return;
						if(location != null) emitter.onSuccess(location);
						else emitter.onError(new UnknownLocationException("Last location is unknown"));
					})
					.addOnFailureListener(emitter::onError);
			}
			catch(SecurityException e)
			{
				emitter.onError(e);
			}
		});
	}
	
	private final FusedLocationProviderClient locationProviderClient;
	private final GoogleApiAvailability googleApiAvailability;
	private final Context context;
	private boolean isGoogleApiAvailable = true;
}
