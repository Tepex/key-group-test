package com.keyg.test.app.presentation;

import android.location.Location;

import android.support.annotation.CallSuper;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;

import com.google.android.gms.maps.model.LatLng;

import com.keyg.test.app.domain.Point;

import com.keyg.test.app.domain.interactor.AddressInteractor;
import com.keyg.test.app.domain.interactor.LocationInteractor;

import com.keyg.test.app.App;
import com.keyg.test.app.R;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import javax.inject.Inject;

import timber.log.Timber;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView>
{
	public MainPresenter()
	{
		super();
		App.getAppComponent().inject(this);
		minDistance = App.getAppComponent().getContext().getResources()
			.getInteger(R.integer.map_min_distance_to_request_address);
		compositeDisposable = new CompositeDisposable();
	}
	
	@Override
	@CallSuper
	public void onDestroy()
	{
		super.onDestroy();
		compositeDisposable.clear();
	}
	
	@Override
	protected void onFirstViewAttach()
	{
		getViewState().isPermissionsGranted(granted ->
		{
			MainPresenter.this.granted = granted;
		});
		Timber.d("granted: "+granted);
		if(!granted) return;
		
		checkLocationServiceAvailability();
		updateCurrentLocation();
	}
	
	private void unsubscribeOnDestroy(Disposable disposable)
	{
		compositeDisposable.add(disposable);
	}
	
	private void checkLocationServiceAvailability()
	{
		Disposable googleDisposable = locationInteractor.checkLocationServicesAvailability()
			.filter(available -> !available)
			.flatMap(available -> locationInteractor.getLocationServicesStatus()
				.toMaybe()
				.subscribeOn(schedulersProvider.io())
				.observeOn(schedulersProvider.ui()))
//				.subscribe(status -> getViewState().showGoogleApiMessage(status), Timber::e);

			.subscribeOn(schedulersProvider.io())
			.observeOn(schedulersProvider.ui())
			.subscribe(status -> Timber.i("Google API status: "+status), Timber::e);
		unsubscribeOnDestroy(googleDisposable);
	}
	
	/**
	 * - Запросить из репозитория текущее положение
	 */
	void updateCurrentLocation()
	{
		Timber.d("update current location");
		getViewState().blockInterface(true);
		Disposable disposable = locationInteractor.getCurrentLocation()
			.doOnSubscribe(disp -> blockInterface(true))
			.doAfterTerminate(() -> blockInterface(false))
			.map(point -> new LatLng(point.getLatitude(), point.getLongitude()))
			.subscribeOn(schedulersProvider.io())
			.observeOn(schedulersProvider.ui())
			.subscribe(this::onCurrentLocationChanged, Timber::e);
		unsubscribeOnDestroy(disposable);
	}
	
	/**
	 * Запросить адрес
	 */
	void move(LatLng latLng)
	{
		getViewState().setMapPoint(latLng);
		requestAddress(latLng);
	}
	
	private void onCurrentLocationChanged(LatLng latLng)
	{
		Timber.d("onCurrentLocationChanged: "+latLng);
		getViewState().setMapPoint(latLng);
		requestAddress(latLng);
	}
	
	private void onAddressAnswer(String address)
	{
		if(!cachedAddress.equals(address))
		{
			getViewState().setAddress(address);
			cachedAddress = address;
		}
	}
	
	private void blockInterface(boolean block)
	{
		getViewState().blockInterface(block);		
	}
	
	
	
	private void requestAddress(LatLng latLng)
	{
		Point point = createPoint(latLng);
		/* Не запрашивать адрес, если перемещение составило менее minDistance */
		float dx = getDistance(point);
		if(dx < minDistance) return;
		lastPoint = point;
		
		Disposable disposable = addressInteractor.getAddressByLocation(point)
			.subscribeOn(schedulersProvider.io())
			.observeOn(schedulersProvider.ui())
			.subscribe(this::onAddressAnswer, Timber::e);
		unsubscribeOnDestroy(disposable);
	}
	
	private float getDistance(Point point)
	{
		float[] distance = new float[2];
		Location.distanceBetween(lastPoint.getLatitude(), lastPoint.getLongitude(),
			point.getLatitude(), point.getLongitude(), distance);
		return distance[0];
	}
	
	private static Point createPoint(LatLng latLng)
	{
		return new Point(latLng.latitude, latLng.longitude);
	}
	
	//private void showErrorMessage(ErrorBundle errorBundle) {}

	private final CompositeDisposable compositeDisposable;
	@Inject
	LocationInteractor locationInteractor;
	@Inject
	AddressInteractor addressInteractor;
	@Inject
	SchedulersProvider schedulersProvider;
	private boolean granted;
	private Point lastPoint = new Point();
	private final int minDistance;
	private String cachedAddress = "";
}
