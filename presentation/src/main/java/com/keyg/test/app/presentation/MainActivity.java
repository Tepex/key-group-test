package com.keyg.test.app.presentation;

import android.Manifest;

import android.content.Intent;
import android.content.pm.PackageManager;

import android.os.Bundle;
import android.os.Build;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import android.support.design.widget.Snackbar;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.View;

import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.keyg.test.app.App;
import com.keyg.test.app.R;

import timber.log.Timber;

public class MainActivity extends MvpAppCompatActivity implements MainView
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			permissionsGranted = checkPermission();
		
		setContentView(R.layout.activity_main);
		unbinder = ButterKnife.bind(this);
		toolbar.setTitle(R.string.app_name);
		
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		
		Bundle mapViewBundle = null;
		if(savedInstanceState != null)
		{
			isFirst = false;
			mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
		}
		
		Timber.d("Permissions granted: "+permissionsGranted);
		if(permissionsGranted) startGoogleMap(mapViewBundle);
		else Snackbar.make(rootView, "Permissions not granted", Snackbar.LENGTH_SHORT).show();
	}
	
	private void startGoogleMap(Bundle mapViewBundle)
	{
		mapView.onCreate(mapViewBundle);
		mapView.getMapAsync(gmap -> 
		{
			this.gmap = gmap;
			this.gmap.setMyLocationEnabled(true);
			this.gmap.setOnMyLocationButtonClickListener(() ->
			{
				onClickMyLocation();
				return true;
			});
			this.gmap.setOnCameraMoveListener(this);
			presenter.updateCurrentLocation();
		});
	}
	
	@Override
	@CallSuper
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
		if(mapViewBundle == null)
		{
			mapViewBundle = new Bundle();
			outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
		}
		if(permissionsGranted) mapView.onSaveInstanceState(mapViewBundle);
	}
	
	@Override
	@CallSuper
	protected void onStart()
	{
		super.onStart();
		if(permissionsGranted) mapView.onStart();
	}
	
	@Override
	@CallSuper
	protected void onResume()
	{
		super.onResume();
		if(permissionsGranted) mapView.onResume();
	}
	
	@Override
	@CallSuper
	protected void onPause()
	{
		if(permissionsGranted) mapView.onPause();
		super.onPause();
	}
	
	@Override
	@CallSuper
	protected void onStop()
	{
		if(permissionsGranted) mapView.onStop();
		super.onStop();
	}
		
	@CallSuper
	@Override
	protected void onDestroy()
	{
		if(permissionsGranted) mapView.onDestroy();
		if(unbinder != null) unbinder.unbind();
		unbinder = null;
		super.onDestroy();
	}
	
	@Override
	@CallSuper
	public void onLowMemory()
	{
		if(permissionsGranted) mapView.onLowMemory();
		super.onLowMemory();
	}
	
	/**
	 * @return true — не требуется разрешение пользователя
	 */
	private boolean checkPermission()
	{
		if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
		   ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
		{
			ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST);
			return false;
		}
		return true;
	}
	
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
	{
		if(requestCode != PERMISSION_REQUEST) return;
		if(grantResults.length == 2 && 
			grantResults[0] == PackageManager.PERMISSION_GRANTED &&
			grantResults[1] == PackageManager.PERMISSION_GRANTED) recreate();
		else finish();
	}
	

	
	
	/** View */
	
	@Override
	public void blockInterface(boolean block)
	{
	}
	
	@Override
	public void setMapPoint(LatLng current)
	{
		if(gmap != null)
		{
			if(centerMarker == null)
			{
				/* Грязный хак!!! */
				if(isFirst || gmap.getCameraPosition().zoom <= 2.0)
				{
					int zoom = getResources().getInteger(R.integer.map_min_zoom);
					gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(current, zoom));
				}
				else gmap.moveCamera(CameraUpdateFactory.newLatLng(current));
				centerMarker = gmap.addMarker(new MarkerOptions().position(current));
			}
			else
			{
				gmap.moveCamera(CameraUpdateFactory.newLatLng(current));
				centerMarker.setPosition(current);
			}
		}
	}
	
	@Override
	public void setAddress(String address)
	{
		tvAddress.setText(address);
	}
	
	@Override
	public void onCameraMove()
	{
		LatLng centerPoint = gmap.getCameraPosition().target;
		presenter.move(centerPoint);
	}
	
	@Override
	public void onClickMyLocation()
	{
		presenter.updateCurrentLocation();
	}
	
	@Override
	public void isPermissionsGranted(Callback callback) 
	{
		callback.call(permissionsGranted);
	}
	
	@InjectPresenter
	MainPresenter presenter;
	
	Unbinder unbinder;
	@BindView(R.id.root) View rootView;
	@BindView(R.id.toolbar) Toolbar toolbar;
	@BindView(R.id.mapview) MapView mapView;
	@BindView(R.id.address) TextView tvAddress;
	
	private boolean permissionsGranted = true;
	private boolean isFirst = true;
	private GoogleMap gmap;
	private Marker centerMarker;
	
	private static final int PERMISSION_REQUEST = 2211;
	private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
	private static final String[] PERMISSIONS = {
		Manifest.permission.ACCESS_FINE_LOCATION, 
		Manifest.permission.ACCESS_COARSE_LOCATION}; 
}
