package com.keyg.test.app;

import android.app.Application;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.keyg.test.app.di.AppComponent;
import com.keyg.test.app.di.DaggerAppComponent;
import com.keyg.test.app.di.ContextModule;

import timber.log.Timber;

public class App extends Application
{
	@Override
	@CallSuper
	public void onCreate()
	{
		super.onCreate();
		appComponent = DaggerAppComponent.builder()
			.contextModule(new ContextModule(this))
			.build();

		if(BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());
	}
	
	public static AppComponent getAppComponent() 
	{
		return appComponent;
	}

	@VisibleForTesting
	public static void setAppComponent(@NonNull AppComponent ac) 
	{
		appComponent = ac;
	}
	
	private static AppComponent appComponent;
}
