package com.keyg.test.app.di;

import android.content.Context;

import com.keyg.test.app.presentation.MainPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ContextModule.class)
public interface AppComponent 
{
	Context getContext();
	void inject(MainPresenter presenter);
}
