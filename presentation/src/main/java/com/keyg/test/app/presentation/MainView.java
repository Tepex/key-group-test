package com.keyg.test.app.presentation;

import com.arellomobile.mvp.MvpView;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import com.google.android.gms.maps.GoogleMap.OnCameraMoveListener;

import com.google.android.gms.maps.model.LatLng;

@StateStrategyType(OneExecutionStateStrategy.class)
public interface MainView extends MvpView, OnCameraMoveListener
{
	void blockInterface(boolean block);
	
	/**
	 * Установка маркера и камеры.
	 */
	void setMapPoint(LatLng point);
	void setAddress(String address);
	void onClickMyLocation();
	void isPermissionsGranted(Callback callback);
	
	public static interface Callback
	{
		void call(boolean granted);
	}
}
