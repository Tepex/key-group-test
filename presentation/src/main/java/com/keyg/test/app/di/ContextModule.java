package com.keyg.test.app.di;

import android.content.Context;
import android.location.Geocoder;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import com.keyg.test.app.domain.interactor.AddressInteractor;
import com.keyg.test.app.domain.interactor.LocationInteractor;
import com.keyg.test.app.domain.repository.AddressRepository;
import com.keyg.test.app.domain.repository.LocationRepository;

import com.keyg.test.app.data.repository.DataAddressRepository;
import com.keyg.test.app.data.repository.DataLocationRepository;
import com.keyg.test.app.presentation.SchedulersProvider;

import com.google.android.gms.common.GoogleApiAvailability;

import java.util.Locale;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule
{
	public ContextModule(Context context) 
	{
		this.context = context;
	}

	@Provides
	@Singleton
	public Context provideContext() 
	{
		return context;
	}
	
	@Provides
	@Singleton
	public SchedulersProvider provideSchedulersProvider()
	{
		return new SchedulersProvider();
	}
	
	@Provides
	@Singleton
	FusedLocationProviderClient provideLocationClient()
	{
		return LocationServices.getFusedLocationProviderClient(context);
	}

	@Provides
	@Singleton
	LocationRepository provideLocationRepository(FusedLocationProviderClient locationProviderClient,
                                                 GoogleApiAvailability googleApiAvailability,
                                                 Context context)
	{
		return new DataLocationRepository(locationProviderClient, 
			                              googleApiAvailability,
			                              context);
	}
	
	@Provides
	@Singleton
	Locale provideLocale()
	{
		return Locale.getDefault();
	}
	
	@Provides
	@Singleton
	Geocoder provideGeocoder(Context context, Locale locale)
	{
		return new Geocoder(context, locale);
	}
	
	@Provides
	@Singleton
	AddressRepository provideAddressRepository(Geocoder geocoder)
	{
		return new DataAddressRepository(geocoder);
	}
	
	@Provides
	@Singleton
	LocationInteractor provideLocationInteractor(LocationRepository repository)
	{
		return new LocationInteractor(repository);
	}
	
	@Provides
	@Singleton
	AddressInteractor provideAddressInteractor(AddressRepository repository)
	{
		return new AddressInteractor(repository);
	}
	
	@Provides
	@Singleton
	GoogleApiAvailability provideGoogleApiAvailability() 
	{
		return GoogleApiAvailability.getInstance();
	}
	
	private Context context;
}
