План дальнейшего развития
=========================
## 1. Разбивка проекта на 3 части по слоям ClArch
[Годная статья](https://proandroiddev.com/creating-clean-architecture-multi-project-mvp-app-34d753a187ad)

++ Gradle: вынос общих зависимостей в корень
++ 1.1 data
++ 1.1.1 прописать зависимость 
++ 1.2 presentation
++ 1.3 domain

## 2. Переход на Kotlin
## 3. Тестирование
## 4. Coroutines
[Серия статей Coroutines vs RxJava]()
