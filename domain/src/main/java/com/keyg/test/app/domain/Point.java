package com.keyg.test.app.domain;

public class Point
{
	public Point()
	{
		this(0.0d, 0.0d);
	}
	
	public Point(double latitude, double longitude)
	{
		this.latitude = latitude;
		this.longitude = longitude;
		str = new StringBuilder()
			.append("[")
			.append(latitude)
			.append(", ")
			.append(longitude)
			.append("]")
			.toString();
	}
	
	public double getLatitude()
	{
		return latitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
	
	@Override
	public String toString()
	{
		return str;
	}
	
	private final double latitude;
	private final double longitude;
	private final String str;
}
