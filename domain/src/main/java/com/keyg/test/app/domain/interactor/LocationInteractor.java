package com.keyg.test.app.domain.interactor;

import com.keyg.test.app.domain.Point;
import com.keyg.test.app.domain.repository.LocationRepository;

import io.reactivex.Single;

public class LocationInteractor
{
	public LocationInteractor(LocationRepository repository)
	{
		this.repository = repository;
	}
	
	public Single<Point> getCurrentLocation()
	{
		return repository.getCurrentLocation();
	}
	
	public Single<Boolean> checkLocationServicesAvailability()
	{
		return Single.just(repository.checkPlayServicesAvailable());
	}
	
	public Single<Integer> getLocationServicesStatus()
	{
		return Single.just(repository.getPlayServicesStatus());
	}
	
	private final LocationRepository repository;
}
