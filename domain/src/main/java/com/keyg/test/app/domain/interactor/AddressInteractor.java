package com.keyg.test.app.domain.interactor;

import com.keyg.test.app.domain.Point;
import com.keyg.test.app.domain.repository.AddressRepository;

import io.reactivex.Single;

public class AddressInteractor
{
	public AddressInteractor(AddressRepository repository)
	{
		this.repository = repository;
	}
	
	public Single<String> getAddressByLocation(Point point)
	{
		return repository.getAddressByLocation(point);
	}
	
	private final AddressRepository repository;
}
