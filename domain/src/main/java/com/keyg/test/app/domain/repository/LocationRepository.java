package com.keyg.test.app.domain.repository;

import com.keyg.test.app.domain.Point;

import io.reactivex.Single;

public interface LocationRepository
{
	Single<Point> getCurrentLocation();
	boolean checkPlayServicesAvailable();
	int getPlayServicesStatus();
}
