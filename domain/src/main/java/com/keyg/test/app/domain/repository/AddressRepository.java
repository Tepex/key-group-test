package com.keyg.test.app.domain.repository;

import com.keyg.test.app.domain.Point;

import io.reactivex.Single;

public interface AddressRepository
{
	Single<String> getAddressByLocation(Point point);
}
